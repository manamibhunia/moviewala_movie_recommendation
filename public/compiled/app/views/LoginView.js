(function() {
  var _ref,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  window.LoginView = (function(_super) {
    __extends(LoginView, _super);

    function LoginView() {
      _ref = LoginView.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    LoginView.prototype.template = '<div class="row" style="margin-top:100px;">\
      <div class="col-lg-1"></div>\
      <div class="col-lg-6">\
        <h1 class="coverheader">MovieWala</h1>\
        <blockquote style="font-size: 1.5em; color: #6f6f8e;"><em>Confused what to watch ?? </em> No worries! movie recommendations from people like you!</blockquote>\
        <h1></h1>\
        <div class="panel panel-default">\
          <div class="input-group">\
            <input type="text" class="form-control" placeholder=" What name do you prefer? ">\
            <span class="input-group-btn">\
              <button id="submitButton" class="btn btn-primary" type="button">Go</button>\
              </a>\
            </span>\
          </div>\
        </div>\
        <div class="col-lg-4"></div>\
      </div>\
    </div>\
    <div class="row" style="margin-top:50px;">\
      <div class="col-lg-2"></div>\
      <div class="col-lg-8">\
      </div>\
      <div class="col-lg-2"></div>\
    </div>';

    LoginView.prototype.initialize = function() {
      this.render();
      return $(document).ready(function() {
        var mosimg;
        mosimg = new Image();
        return mosimg.src = "/img/moviecollage2.png";
      });
    };

    LoginView.prototype.events = {
      "click #submitButton": 'getUser',
      "keyup :input": 'checkEnter'
    };

    LoginView.prototype.checkEnter = function(e) {
      console.log(e);
      if (e.which === 13) {
        return this.getUser();
      }
    };

    LoginView.prototype.getUser = function() {
      var _this = this;
      this.username = this.$('input').val().trim();
      if(!this.username) {
        return false;
      }
      _(this.model).extend({
        name: this.username
      });
      return this.model.fetch({
        error: function(model, response) {
          return console.log('model', model);
        },
        success: function(model, response) {
          return _this.userInfoReceived(response);
        }
      });
    };

    LoginView.prototype.userInfoReceived = function(userObject) {
      return this.trigger('userInfoReceived', userObject);
    };

    LoginView.prototype.render = function() {
      return this.$el.append(this.template);
    };

    return LoginView;

  })(Backbone.View);

}).call(this);
