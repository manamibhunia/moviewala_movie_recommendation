(function() {
  var _ref,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  window.TopRatedView = (function(_super) {
    __extends(TopRatedView, _super);

    function TopRatedView() {
      _ref = TopRatedView.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    TopRatedView.prototype.template = '\
      <div class="top-rated">\
      <ul id="toprated">\
      </ul>\
      <div id="top-rated-modal" class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
      <h4 class="modal-title header-label h4"></h4></div><div class="modal-body"><div class="body-content"><div class="col-md-4 poster"></div><div class="col-md-8 attributes"></div></div></div></div></div></div>\
      </div>\
      ';

    TopRatedView.prototype.initialize = function() {
      this.oldRated;
      this.$el.append(this.template);
      this.$('.tip5').tooltip('hide');
    };

    TopRatedView.prototype.events = {
      "click .movie-item": function(e) {
        console.log('$(e.target)- ',$(e.target));
        var movieName = $(e.target).closest('li').attr('data-movieName');
        var url = 'http://www.omdbapi.com/?t=' + movieName + '&y=&plot=short&r=json';
        $('#top-rated-modal .body-content .attributes').html('');
        $('#top-rated-modal .header-label').html(movieName);
        $.getJSON( url, function( data ) {
          var items = [];
          $.each( data, function( key, val ) {
            if(key !== 'Poster') {
              if(key !== 'imdbID' && key !== 'Type' && key !== 'Response') {

                if(key !== 'imdbRating') {
                  items.push( "<li id='" + key + "'><span><label>" + key + ": </label></span><span>" + val + "</span></li>" );
                } else {

                  var positiveRating = (val * 10);
                  var negativeRating = (10 - val) * 10;

                  var progressBar = '<div class="progress"><div class="progress-bar progress-bar-success" style="width: ' + positiveRating + '%"><span class="sr-only"></span></div>' + 
                    '<div class="progress-bar progress-bar-danger progress-bar-striped" style="width: ' + negativeRating + '%"><span class="sr-only"></span></div></div>';
                  items.push( "<li id='" + key + "'><span><label>" + key + ": </label></span><span>" + val + "</span>" + progressBar + "</li>" );
                }
              }
            } else {
              $( "#top-rated-modal .body-content .poster" ).html('<img src="' + val + '">');
            }
          });
         
          $( "<ul/>", {
            "class": "movie-attribute-list",
            html: items.join( "" )
          }).appendTo( "#top-rated-modal .body-content .attributes" );
          $( "#top-rated-modal .body-content .poster" ).append('<p>Lets check what people are saying about this movie in <a href="">Twitter</a></p>');
          
          $('#top-rated-modal').modal('show');
        });
      }
    };

    TopRatedView.prototype.translateRes = function(res) {
      var index, value, _ref1;
      this.scoreImport = res.bestScores;
      this.movieArray = [];
      this.scoreArray = [];
      this.scoreLength = this.scoreImport.length;
      _ref1 = this.scoreImport;
      for (index in _ref1) {
        value = _ref1[index];
        if (index % 2 === 0) {
          this.movieArray.push(value);
        } else {
          this.scoreArray.push(value);
        }
      }
      return this.reRender();
    };

    TopRatedView.prototype.reRender = function() {
      var index, movieid, newMovie, _ref1;
      $('#toprated').html('');
      _ref1 = this.movieArray;
      for (index in _ref1) {
        movieid = _ref1[index];
        this.movie = this.model.userObj.movieLookup[movieid] || 'newMovie';
        newMovie = $('<li class="movie-item" data-movieName="' + this.movie + '"><div id="' + movieid + '" class="element sprites ' + this.movie.replace(/\s+/g, '').toLowerCase() + '"></div><div class="movie-label">' + this.movie + '</br><div class="rating">' + this.scoreArray[index].substring(0, 4) + '</div></div></li>');
        $('#toprated').append(newMovie);
      }
    };

    return TopRatedView;

  })(Backbone.View);

}).call(this);
