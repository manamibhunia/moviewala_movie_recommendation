(function() {
  var _ref,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  window.RecommendationView = (function(_super) {
    __extends(RecommendationView, _super);

    function RecommendationView() {
      _ref = RecommendationView.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    RecommendationView.prototype.template = '\
      <div class="right-panel panel panel-default"><div class="panel-heading"><h5 class="h5">Recommendations\
      <a class="tip2" data-toggle="tooltip" data-placement="left" title="Here are your recommendations! They change after every new rating based on what similar users like and dislike.">\
        <i class="icon-info-sign smallicon"></i>\
      </a></h5></div><div class="panel-body">\
      <div class="scrollBox">\
      <div id="content"><ul id="tabs" class="nav nav-tabs" data-tabs="tabs"><li class="active"><a href="#top-rated-recommendations" data-toggle="tab">Top Rated</a></li>\
      <li><a href="#top-user-recommendations" data-toggle="tab">User Similarity</a></li><li><a href="#my-recommendations" data-toggle="tab">My Recommendations</a></li></ul>\
      <div id="my-tab-content" class="tab-content"><div class="tab-pane active" id="top-rated-recommendations"></div><div class="tab-pane" id="top-user-recommendations"></div>\
      <div class="tab-pane" id="my-recommendations"><ul id="container"></ul>\
      <div id="recommendation-modal" class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
      <h4 class="modal-title header-label h4"></h4></div><div class="modal-body"><div class="body-content">\
      <div class="col-md-4 poster"></div><div class="col-md-8 attributes"></div>\
      </div></div></div></div></div>\
      </div></div></div></div></div></div>\
      ';

    RecommendationView.prototype.topUsersTemplate = '<div class="topUsers">\
    </div>';

    RecommendationView.prototype.topRatedTemplate = '<div class="topRated">\
    </div>';

    RecommendationView.prototype.loadingTemplate = '<div class="loading">\
      <i class="icon-spinner icon-spin icon-large"></i> please enter more ratings...\
    </div>';

    RecommendationView.prototype.initialize = function() {
      setTimeout(function() {
        return this.$('.tip').tooltip({
          placement: 'auto'
        }).tooltip('show');
      }, 1000);
      this.oldMovies;
      this.initial = false;
      this.$el.append(this.template);
      this.$el.append(this.loadingTemplate);
    };

    RecommendationView.prototype.events = {
      "click .element": function(e) {},
      "click .movie-item": function(e) {
        console.log('$(e.target)- ',$(e.target));
        var movieName = $(e.target).closest('li').attr('data-movieName');
        var url = 'http://www.omdbapi.com/?t=' + movieName + '&y=&plot=short&r=json';
        $('#recommendation-modal .body-content .attributes').html('');
        $('#recommendation-modal .header-label').html(movieName);
        $.getJSON( url, function( data ) {
          var items = [];
          $.each( data, function( key, val ) {
            if(key !== 'Poster') {
              if(key !== 'imdbID' && key !== 'Type' && key !== 'Response') {

                if(key !== 'imdbRating') {
                  items.push( "<li id='" + key + "'><span><label>" + key + ": </label></span><span>" + val + "</span></li>" );
                } else {

                  var positiveRating = (val * 10);
                  var negativeRating = (10 - val) * 10;

                  var progressBar = '<div class="progress"><div class="progress-bar progress-bar-success" style="width: ' + positiveRating + '%"><span class="sr-only"></span></div>' + 
                    '<div class="progress-bar progress-bar-danger progress-bar-striped" style="width: ' + negativeRating + '%"><span class="sr-only"></span></div></div>';
                  items.push( "<li id='" + key + "'><span><label>" + key + ": </label></span><span>" + val + "</span>" + progressBar + "</li>" );
                }
              }
            } else {
              $( "#recommendation-modal .body-content .poster" ).html('<img src="' + val + '">');
            }
          });
         
          $( "<ul/>", {
            "class": "movie-attribute-list",
            html: items.join( "" )
          }).appendTo( "#recommendation-modal .body-content .attributes" );
          $( "#recommendation-modal .body-content .poster" ).append('<p>Lets check what people are saying about this movie in <a href="">Twitter</a></p>');
          $('#recommendation-modal').modal('show');
        });
      }
    };

    RecommendationView.prototype.handleRating = function(ratingObject) {
      var _this = this;
      _(this.model).extend({
        idFetch: ratingObject.id,
        likeFetch: ratingObject.like
      });
      return this.model.fetch({
        error: function(model, response) {
          return console.log('error model', model);
        },
        success: function(model, response) {
          if (_this.initial === false) {
            _this.handleFirstRating();
            _this.initial = true;
          }
          console.log('success res', response);
          return _this.render(response);
        }
      });
    };

    RecommendationView.prototype.handleFirstRating = function() {
      this.initialRender();
      this.$('.loading').hide('slow');
      setTimeout(function() {
        this.$('.tip').tooltip('hide');
        return this.$('.tip2').tooltip({
          placement: 'bottom'
        }).tooltip('show');
      }, 1000);
      return setTimeout(function() {
        return this.$('.tip2').tooltip('hide');
      }, 10000);
    };

    RecommendationView.prototype.initialRender = function() {
      $('#top-user-recommendations').html(this.topUsersTemplate);
      this.topUsersView = new TopUsersView({
        model: this.model
      });
      this.$('.topUsers').html(this.topUsersView.el);
      $('#top-rated-recommendations').html(this.topRatedTemplate);
      this.topRatedView = new TopRatedView({
        model: this.model
      });
      return this.$('.topRated').html(this.topRatedView.el);
    };

    RecommendationView.prototype.render = function(res) {
      var index, movieid, moviesToAdd, moviesToRemove, newMovie, removeMovie;
      this.topUsersView.reRender(res);
      this.topRatedView.translateRes(res);
      moviesToAdd = _.difference(res.recommendations, this.oldMovies);
      moviesToRemove = _.difference(this.oldMovies, res.recommendations);
      this.oldMovies = res.recommendations;

      $('#container').html('');
      for (index in res.recommendations) {
        movieid = res.recommendations[index];
        newMovie = $('<li class="movie-item" data-movieName="' + this.model.userObj.movieLookup[movieid] + '"><div id="' + movieid + '" class="element sprites ' + this.model.userObj.movieLookup[movieid].replace(/\s+/g, '').toLowerCase() + '"></div><div class="movie-label">' + this.model.userObj.movieLookup[movieid] + '</div></li>');
        $('#container').append(newMovie);
      }
      return this;
    };

    return RecommendationView;

  })(Backbone.View);

}).call(this);

