# Movie Wala - personal movie recommendation App

Movie Wala is a personal movie recommendation engine built on top of Node.js. It is a full installation of the module that takes advantage of lightning fast recommendations powered by Redis and a simple user interface.

## Dependencies

* Async
* CSV
* Express
* MongoDB
* Mongoose
* Node
* Raccoon
* Redis
* Underscore
* Twitter Bootstrap
* Font-Awesome

## Repo

* <a href="https://bitbucket.org/manamibhunia/moviewala_movie_recommendation" target="_blank">https://bitbucket.org/manamibhunia/moviewala_movie_recommendation</a>

## How to install locally

#### Clone the repo
```
git clone git@bitbucket.org:manamibhunia/moviewala_movie_recommendation.git
```

#### Navigate to the folder
```
cd movie_recommendation
```

#### Install ALL dependencies
```
npm install
```

#### Make sure Raccoon's sampleContent is true
```
/node_modules/raccoon/lib/config.js
  // make sure - sampleContent: true
```

#### Start servers in separate terminal tabs
```
redis-server
```

```
mongod
```

```
node server.js
```

#### Import Movies from database
* Go to http://localhost:5000/importMovies
* If you're on a different local host, change it out

#### It's ready! Try the home page
* http://localhost:5000/
